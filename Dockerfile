FROM alpine:3.12
RUN apk add --no-cache python3 py3-paramiko py3-pip

RUN pip3 install inotify pytelegraf[http] structlog

WORKDIR /app

ENV SFTP_HOST "sftp-server"
ENV SFTP_PORT "22"
ENV SFTP_USER "upload"
ENV SFTP_PASSWORD ""
ENV UPLOAD_PREFIX "upload/"
ENV PYTHONUNBUFFERED=1

COPY run.py .

ENTRYPOINT ["python3", "run.py"]