#!/usr/bin/env python3

import paramiko
import os
import random
import string
import errno
import time
import inotify.adapters
import inotify.constants
import structlog
import logging
import sys


lg = logging.getLogger("_")
lg.setLevel(logging.DEBUG)

formatter = structlog.stdlib.ProcessorFormatter(
    processor=structlog.processors.JSONRenderer(),
    foreign_pre_chain=[
        structlog.threadlocal.merge_threadlocal,
        structlog.stdlib.filter_by_level,
        structlog.stdlib.add_logger_name,
        structlog.stdlib.add_log_level,
        structlog.stdlib.add_log_level_number,
        structlog.processors.StackInfoRenderer(),
        structlog.processors.format_exc_info,
        structlog.processors.TimeStamper(fmt="iso"),
        structlog.stdlib.PositionalArgumentsFormatter(),
        structlog.processors.UnicodeDecoder()
    ],
    logger=lg
)


handler = logging.StreamHandler(stream=sys.stdout)
handler.setFormatter(formatter)
logging.basicConfig(level=logging.INFO,
                    handlers=[handler])

structlog.configure(
    processors=[
        structlog.stdlib.PositionalArgumentsFormatter(),
        lambda _, __, ed: {'msg': ed.pop("event"), 'stack_info': ed.pop('stack_info', None), 'exc_info': ed.pop('exc_info', None), 'extra': ed}
    ],
    # context_class=dict,
    wrapper_class=structlog.stdlib.BoundLogger,
    logger_factory=structlog.stdlib.LoggerFactory(),
    cache_logger_on_first_use=True,
)

# logger = structlog.wrap_logger(logging.getLogger("sftp"))
logger = structlog.get_logger("sftp")
logger.setLevel(logging.INFO)


"""
environment vars:

SFTP_HOST
SFTP_PORT
SFTP_USER
SFTP_PASSWORD
UPLOAD_PREFIX
"""

upload_prefix = os.environ.get("UPLOAD_PREFIX", "/upload")
watch_dir = os.environ.get("WATCH_DIR", "/data")

sftp_config = {
    'sftp_host': os.environ.get("SFTP_HOST", "localhost"),
    'sftp_port': int(os.environ.get("SFTP_PORT", "2222")),
    'sftp_user': os.environ.get("SFTP_USER", "upload"),
    'password': os.environ.get("SFTP_PASSWORD", "password"),
}
extra_sftp_configs = []


i = 0
while os.environ.get(f"SFTP_HOST_{i}", None):
    config = {
        'sftp_host': os.environ.get(f"SFTP_HOST_{i}", "localhost"),
        'sftp_port': int(os.environ.get(f"SFTP_PORT_{i}", "2222")),
        'sftp_user': os.environ.get(f"SFTP_USER_{i}", "upload"),
        'password': os.environ.get(f"SFTP_PASSWORD_{i}", "password"),
    }
    extra_sftp_configs.append(config)


def construct_remote_file_paths(fp):
    new_fp = fp.replace(watch_dir, "", 1)
    while new_fp.startswith("/"):
        new_fp = new_fp[1:]
    new_fp = os.path.join(upload_prefix, new_fp)
    temp_fn = f'.{"".join([random.choice(string.ascii_lowercase) for _ in range(0, 8)])}.tmp'
    temp_fp = os.path.join(os.path.dirname(new_fp), temp_fn)
    return os.path.normpath(temp_fp), os.path.normpath(new_fp)


def rexists(sftp, path):
    """
    os.path.exists for paramiko's SCP object
    """
    try:
        sftp.stat(path)
    except IOError as e:
        if e.errno == errno.ENOENT:
            return False
        raise
    except Exception:
        raise
    else:
        return True


def upload_file(fp, sftp_host=None, sftp_user=None, sftp_port=None, password=None, delete_after=True):
    if not sftp_host:
        logger.error('got passed none as host arg to upload_file wtf')
        return False

    logger.info(f'uploading')
    try:
        with paramiko.Transport((sftp_host, sftp_port)) as transport:
            transport.connect(username=sftp_user, password=password)
            logger.debug("created transport")

            if not transport.is_authenticated():
                logger.error('transport did not authenticate')
                return True

            with transport.open_sftp_client() as sftp:
                logger.debug("created sftp client")
                temp_fp, remote_fp = construct_remote_file_paths(fp)

                structlog.threadlocal.bind_threadlocal(remote_path=remote_fp,
                                                       temp_path=temp_fp)
                to_check = []
                tdir = temp_fp
                while tdir not in ('/', ''):
                    tdir = os.path.dirname(tdir)
                    to_check.insert(0, tdir)

                for path in to_check:
                    if path in ('/', ''):
                        continue
                    try:
                        if rexists(sftp, path):
                            continue
                        else:
                            sftp.mkdir(path)
                    except Exception as e:
                        logger.error('cannot create parent directories')

                try:
                    logger.debug(f"file upload")
                    sftp.put(fp, temp_fp, confirm=True)
                    logger.debug(f"remote file rename")
                    sftp.posix_rename(temp_fp, remote_fp)
                    if delete_after:
                        os.remove(fp)
                        logger.info(f"removed local file")
                    else:
                        logger.info(f"not deleting local file")
                    logger.info(f'upload successful')
                except Exception as e:
                    logger.error(f'error uploading', exc_info=e)
                    return True
                finally:
                    structlog.threadlocal.unbind_threadlocal('remote_path', 'temp_path')

                return False
    except Exception as e:
        logger.error(f'error creating transport', exc_info=e)
        return True


def cleanup():
    for root, dirnames, filenames in os.walk(watch_dir, topdown=False):
        for dn in dirnames:
            if dn.startswith("."):
                continue
            fp = os.path.realpath(os.path.join(root, dn))

            try:
                os.rmdir(fp)
            except (OSError, FileNotFoundError):
                continue


def try_wait(fn, *args, wait_s=10, tries=10, **kwargs):
    ts = 0
    while fn(*args, **kwargs) and ts < tries:
        ts += 1
        time.sleep(wait_s)


def isf(d, f):
    return os.path.isfile(os.path.join(d, f))


def run_batch():
    logger.debug("running timed batch job")
    file_paths = []
    file_paths.extend([os.path.join(dp, f) for dp, dn, fns in os.walk(watch_dir) for f in fns if isf(dp, f)])
    file_paths = [fp for fp in file_paths if '/.' not in fp]

    if len(file_paths) < 1:
        return

    logger.info(f'batch, uploading {watch_dir}, {len(file_paths)} files to upload')
    for fp in file_paths:
        if os.path.exists(fp):
            filter_upload('BATCH', fp)
    cleanup()


def test_in(check_these, in_array):
    return any(x in in_array for x in check_these)


def filter_upload(type_names, in_fullpath):
    if '/.' in in_fullpath:
        logger.debug('hidden, ignoring', type_names, in_fullpath)
        return

    if not os.path.exists(in_fullpath):
        logger.debug('noexist, ignoring', type_names, in_fullpath)
        return

    logger.debug(f'{type_names}')

    file_paths = []
    if os.path.isdir(in_fullpath):
        file_paths.extend([os.path.join(dp, f) for dp, dn, fns in os.walk(in_fullpath) for f in fns if isf(dp, f)])
    else:
        file_paths.append(in_fullpath)

    for conf in extra_sftp_configs:
        structlog.threadlocal.bind_threadlocal(**conf)
        for fp in file_paths:
            structlog.threadlocal.bind_threadlocal(local_path=fp)
            try_wait(upload_file, fp, delete_after=False, **conf)
            structlog.threadlocal.unbind_threadlocal('local_path')
        structlog.threadlocal.clear_threadlocal()

    structlog.threadlocal.bind_threadlocal(**sftp_config)
    for fp in file_paths:
        structlog.threadlocal.bind_threadlocal(local_path=fp)
        try_wait(upload_file, fp, **sftp_config)
        structlog.threadlocal.unbind_threadlocal('local_path')
    structlog.threadlocal.clear_threadlocal()


def main():
    logger.info("sftp uploader program startup")
    while True:
        run_batch()
        inot = inotify.adapters.InotifyTree(watch_dir)
        # i = inotify.adapters.Inotify()
        # i.add_watch(watch_dir, mask=inotify.constants.IN_CLOSE_WRITE | inotify.constants.IN_MOVED_TO)
        for _ in range(600):
            for ev in inot.event_gen(yield_nones=False, timeout_s=1):
                _, type_names, path, filename = ev
                in_fullpath = os.path.join(path, filename)
                if 'IN_ISDIR' in type_names or ('IN_CLOSE_WRITE' not in type_names and 'IN_MOVED_TO' not in type_names):
                    logger.debug(f"ignored {','.join(type_names)}")
                    continue
                filter_upload(",".join(type_names), in_fullpath)
        # clean up inotify watcher
        del inot


if __name__ == '__main__':
    main()
